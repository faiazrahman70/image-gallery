import React from 'react';
import './Header.css';

export default function Header({ file_no, handleDelete }) {
  return (
    <div className="header">
      <div className="checkbox_c">
        {file_no !== 0 ? (
          <>
            <i className="fa-solid fa-square-check" color="#ce2626"></i>
            <p className="selected_text">
              {file_no <= 1
                ? `${file_no} File Selected`
                : `${file_no} Files Selected`}
            </p>
          </>
        ) : (
          <p className="selected_text">Gallery</p>
        )}
      </div>
      <p className="delete_text" onClick={handleDelete}>
        {file_no <= 1 ? 'Delete file' : 'Delete files'}
      </p>
    </div>
  );
}
