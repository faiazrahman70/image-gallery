import React, { useState, useEffect, useRef } from 'react';
import { Header } from './components';
import './App.css';

import {
  image1,
  image2,
  image3,
  image4,
  image5,
  image6,
  image7,
  image8,
  image9,
  image10,
  image11,
} from './assets';

function App() {
  const images = [
    image1,
    image2,
    image3,
    image4,
    image5,
    image6,
    image7,
    image8,
    image9,
    image10,
    image11,
  ];

  const [galleryImages, setGalleryImages] = useState(images);

  const [draggedItemIndex, setDraggedItemIndex] = useState();
  const [dragOverItemIndex, setDragOverItemIndex] = useState();

  const [checkedList, setCheckedList] = useState([]);

  const handleCheckedItems = (index) => {
    if (checkedList.includes(index)) {
      let new_list = checkedList.filter((val) => val !== index);

      setCheckedList(new_list);
    } else {
      setCheckedList([...checkedList, index]);
    }
  };

  useEffect(() => {
    console.log('checked_elements', checkedList);
  }, [checkedList]);

  const onDragStart = (e, index) => {
    // e.preventDefault();
    console.log('start', index);
    setDraggedItemIndex(index);
  };

  const onDragEnter = (e, index) => {
    // e.preventDefault();
    console.log('enter_', index);
    e.target.value;
    setDragOverItemIndex(index);
  };

  const handleSorting = () => {
    let mutable_list = [...galleryImages];

    let tmp;
    tmp = mutable_list[draggedItemIndex];
    mutable_list[draggedItemIndex] = mutable_list[dragOverItemIndex];
    mutable_list[dragOverItemIndex] = tmp;

    setGalleryImages(mutable_list);
  };

  const handleDelete = () => {
    const temp = [];

    galleryImages.forEach((item, ind) => {
      if (!checkedList.includes(ind)) {
        temp.push(item);
      }
    });

    setGalleryImages(temp);
    setCheckedList([]);
  };

  useEffect(() => {
    console.log('current_value: ', draggedItemIndex, dragOverItemIndex);
  }, [draggedItemIndex, dragOverItemIndex]);

  return (
    <>
      <div className="app">
        <div className="gallery_c">
          <Header file_no={checkedList.length} handleDelete={handleDelete} />
          <div className="gallery">
            {galleryImages.map((item, ind) => {
              return (
                <div
                  key={ind}
                  draggable={true}
                  onDragStart={(e) => onDragStart(e, ind)}
                  onDragEnter={(e) => onDragEnter(e, ind)}
                  onDrop={handleSorting}
                  style={{
                    transition: `all 1s ease-in`,
                  }}
                  onDragOver={(e) => {
                    e.preventDefault();
                  }}
                  className={ind == 0 ? 'image1' : 'image2'}>
                  <img src={item} alt={`${item}`} className="img" />
                  <div
                    className={
                      checkedList.includes(ind)
                        ? ind === 0
                          ? 'image1 selected'
                          : 'image2 selected'
                        : 'image1 cover'
                    }>
                    <div className="checkbox_wrapper">
                      <input
                        id="check"
                        type="checkbox"
                        checked={checkedList.includes(ind) ? true : false}
                        onChange={() => handleCheckedItems(ind)}
                      />
                    </div>
                  </div>
                </div>
              );
            })}
            <div className="add_image_c">
              <i className="fa-regular fa-image"></i>
              <p className="add_image_text">Add Images</p>
            </div>
          </div>
        </div>
      </div>
    </>
  );
}

export default App;
